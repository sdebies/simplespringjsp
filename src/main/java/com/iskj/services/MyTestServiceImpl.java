package com.iskj.services;

import org.springframework.stereotype.Service;

@Service("myTestService")
public class MyTestServiceImpl implements MyTestService {

    public String test(String str){
        return "my return value " + str;
    }
}
