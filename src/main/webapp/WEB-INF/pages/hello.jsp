<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" uri="http://localhost/diet/skjtaglib" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<html>
  <body>
    <h1>Hello Page</h1>
    <h3>Message : ${message}</h3>
    <h3>Global Count: ${GlobalCount}</h3>
    <h3>Session Count: ${SessionCount}</h3>
    <t:test name="something"/>
    <c:if test="${GlobalCount == 4}">The Global count is four</c:if>
    <br/>
    <p>Internationalization:
      <fmt:message key="first.name"/>
    </p>
  </body>
</html>
